<?php
/*
 *  Copyright 2014-2016 Milton Pividori, Juan Pablo Vidocevich, Diego Milone and Georgina Stegmayer.
 *
 *  This file is part of Web-demo builder.
 *
 *  Web-demo builder is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Web-demo builder is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with Web-demo builder. If not, see <http://www.gnu.org/licenses/>.
 *
 */

//upload zip and parse source code
//R

require 'upload_zip.php';

function process_file($file) {
    $funciones = array();
    while(!feof($file))
    {
        $linea = fgets($file);

        if (strpos($linea, "#")=== false and strpos($linea, "function")!== false){
            $linea=str_replace("{","",$linea);
            $linea=str_replace("(","",$linea);
            $linea=str_replace(")","",$linea);
            $linea=str_replace("=","",$linea);
            $linea=str_replace("<-","",$linea);
            $linea=str_replace(" ","",$linea);
            $linea=explode("function",$linea);
            $outputs=$linea[0];
            $fun=$linea[0];
            $inputs=$linea[1];
            $inputs=explode(",",$inputs);
            $funcion["name"] = $fun;
            for ($j=0;$j<count($inputs);$j++){
                $funcion["inputs"][$j] = $inputs[$j];
            }
            $funcion["outputs"][0] = $outputs;
            $funciones[]=$funcion;
            $funcion = array();
        }
    }
    fclose($file);
    return $funciones;
}

$res = upload_zip("r", 'process_file');
echo json_encode($res);
die();
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>Untitled Document</title>
</head>

<body>
<?php if(isset($message)) echo "<p>$message</p>"; ?>
<form enctype="multipart/form-data" method="post" action="">
<label>Choose a zip file to upload: <input type="file" name="fileinput-script" /></label>
<br />
<input type="hidden" name="id_demo" value="test" />
<input type="submit" name="submit" value="Upload" />
</form>
</body>
</html>
