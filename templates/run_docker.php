<?php
/*
 *  Copyright 2014-2016 Milton Pividori, Juan Pablo Vidocevich, Diego Milone and Georgina Stegmayer.
 *
 *  This file is part of Web-demo builder.
 *
 *  Web-demo builder is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Web-demo builder is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with Web-demo builder. If not, see <http://www.gnu.org/licenses/>.
 *
 */

function get_tmp_script_dir($id_demo, $session) {
  return '/wdb_scripts/'.$id_demo.'_'.$session;
}

function run_docker_command($docker_command, $id_demo, $session, $directory) {
  exec($docker_command, $res, $status);
  $out = join('<br/>',$res);
  $out = 'Command:<br/>'.$docker_command.'<br/><br/>'.'Console response:<br/>'.$out;

  if ($status > 0){
    $ret[0] = $status;
    $ret[1] = base64_encode($out);
    return $ret;
  }
  // Put an image ID and remove the tag
  $file = fopen("demo.json","r");
  $demo_json = stream_get_contents($file);
  fclose($file);
  $demo=json_decode($demo_json);

  exec("docker inspect --format '{{.Id}}' ".$demo->virtual_env, $res2, $sta);
  if ($sta == 0 && $demo->virtual_env !== $res2[0]) {
    $demo->virtual_env = $res2[0];
    $demo_json = json_encode($demo, JSON_PRETTY_PRINT);

    $file = fopen("demo.json","w+");
	  fwrite($file, $demo_json);
	  fclose($file);
  }

  $tmp_dir = get_tmp_script_dir($id_demo, $session);
  exec('cp -pr '.$tmp_dir.'/* '.$directory);
  
  $ret[0] = $status;
  $ret[1] = base64_encode($out);
  return $ret;
}

function recurse_copy($src,$dst) { 
    $dir = opendir($src); 
    @mkdir($dst); 
    while(false !== ( $file = readdir($dir)) ) { 
        if (( $file != '.' ) && ( $file != '..' )) { 
            if ( is_dir($src . '/' . $file) ) { 
                recurse_copy($src . '/' . $file,$dst . '/' . $file); 
            } 
            else { 
                copy($src . '/' . $file,$dst . '/' . $file); 
            } 
        } 
    } 
    closedir($dir); 
}

function get_docker_command($session, $directory, $id_demo, $image, $run_script, $extra_mount) {
  $app_directory = get_tmp_script_dir($id_demo, $session);
  exec('cp -pr '.$directory.' '.$app_directory);

  $dir_mount = '';
  if (!is_null($extra_mount)) {
    $dir_mount = '-v '.escapeshellarg($extra_mount);
  }

  $docker_command = 'docker run --rm '.$dir_mount.' -v '.escapeshellarg($app_directory.':/app/').' '.escapeshellarg($image).' /bin/bash '.escapeshellarg('/app/'.$run_script).' 2>&1';

  return $docker_command;
}

?>
