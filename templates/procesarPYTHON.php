<?php
/*
 *  Copyright 2014-2016 Milton Pividori, Juan Pablo Vidocevich, Diego Milone and Georgina Stegmayer.
 *
 *  This file is part of Web-demo builder.
 *
 *  Web-demo builder is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Web-demo builder is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with Web-demo builder. If not, see <http://www.gnu.org/licenses/>.
 *
 */

require 'run_docker.php';

//PYTHON CONNECTOR

//crea archivo .py para conectar codigo fuente con formulario web y envia a python para su procesamiento
try{
$wdb_config = parse_ini_file("config.ini");

$PYTHON_Path = $wdb_config["python_path"];

$file = fopen("demo.json","r");
$demo_json = stream_get_contents($file);
fclose($file);
$demo=json_decode($demo_json);

$sesion =substr(str_shuffle('abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ'),0, 8);

$file_output = $sesion.".json";
$namespace = trim($demo->filescript);
$namespace = substr($namespace, 0, -3); 

$PYTHON_variables = "import ".$namespace.PHP_EOL;

for ($i=0;$i<count($demo->inputs);$i++){
    $input = $_REQUEST["input".($i+1)]; 
	if (is_numeric($input))
		$PYTHON_variables .= "input".($i+1)."=".$input.PHP_EOL;
	else
		$PYTHON_variables .= "input".($i+1)."='".$input."'".PHP_EOL;
}
for ($i=0;$i<count($demo->outputs);$i++){
	if ($i==0)
		$PYTHON_variables.="output".($i+1);
	else
		$PYTHON_variables.=",output".($i+1);
}
$PYTHON_variables .= "=".$namespace.'.'.$demo->function;

$PYTHON_variables .= "(";
for ($i=0;$i<count($demo->inputs);$i++){
	if ($i==0)
		$PYTHON_variables.="input".($i+1);
	else
		$PYTHON_variables.=",input".($i+1);
}
$PYTHON_variables .= ")".PHP_EOL;

$PYTHON_variables.="archi=open('".$file_output."','a')".PHP_EOL;
$PYTHON_variables.="archi.write('{')".PHP_EOL;

for ($i=0;$i<count($demo->outputs);$i++){
 	if ($i==0)
		$PYTHON_variables.="archi.write('\"output".($i+1)."\":\"'+str(output".($i+1).")+'\"')".PHP_EOL;
	else
		$PYTHON_variables.="archi.write(',\"output".($i+1)."\":\"'+str(output".($i+1).")+'\"')".PHP_EOL;
}
$PYTHON_variables.="archi.write('}')".PHP_EOL;
$PYTHON_variables.="archi.close()".PHP_EOL;

$file2 = fopen($sesion.".py","w+");
fwrite($file2, $PYTHON_variables .PHP_EOL);
fclose($file2);

$comando = $PYTHON_Path.' '.$sesion.'.py || python '.$sesion.'.py';

// prepare to run inside a Docker container
$run_file = 'run.sh';
$run_file_out = 'run'.$sesion.'.sh';

$file_contents = file_get_contents($run_file);
$file_contents = str_replace('%WDB_COMMAND%',$comando,$file_contents);
file_put_contents($run_file_out,$file_contents);

$docker_command = get_docker_command($sesion, __DIR__, $demo->id_demo, $demo->virtual_env, $run_file_out, NULL);
$status = run_docker_command($docker_command, $demo->id_demo, $sesion, __DIR__);

if ($status[0] > 0) {
  echo '{"error":"There was an error when running the virtual environment. It might have been set up incorrectly.","err_det":"'.$status[1].'"}';
  return;
}

$file3 = fopen($file_output,"r");
$output_json = stream_get_contents($file3);
fclose($file3);
unlink($sesion.".py");
unlink($file_output);
echo $output_json;
}
catch (Exception $e) {
    echo '{"error":"Connector Error!","err_det":"'.base64_encode($e->getMessage()).'"}';
}
?>
