<?php
/*
 *  Copyright 2014-2016 Milton Pividori, Juan Pablo Vidocevich, Diego Milone and Georgina Stegmayer.
 *
 *  This file is part of Web-demo builder.
 *
 *  Web-demo builder is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Web-demo builder is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with Web-demo builder. If not, see <http://www.gnu.org/licenses/>.
 *
 */

//load webdemo json
$file = fopen("demo.json","r");
$demo_json = stream_get_contents($file);
$demo=json_decode($demo_json);
?>
<!DOCTYPE html>
<html style="height: 100%; width: 100%;" lang="en"><head>
<meta http-equiv="content-type" content="text/html; charset="UTF-8">
<meta charset="utf-8">

<link rel="stylesheet" href="style.css">
<link rel="stylesheet" media="all" type="text/css" href="jquery-impromptu.min.css" />
<script type="text/javascript" src="jquery.js"></script>
<script type="text/javascript" src="jquery.ajaxLoader.js"></script>
<script type="text/javascript" src="jquery-impromptu.min.js"></script>
<script type="text/javascript">

function procesar(){
	      var loaderContainer = $('div#content');
		  loaderContainer.ajaxLoader();
		  <?php
		  $var_inputs = "";
		  for ($i=0;$i<count($demo->inputs);$i++){
			if ($i==0) {
				if ($demo->inputs[$i]->type=="check")
					$var_inputs .='"input1="+($("#input1").prop("checked") ? 1 : 0)';				
				else			
					$var_inputs .='"input1="+$("#input1").val()';
			}
			else {
				if ($demo->inputs[$i]->type=="check")
					$var_inputs .='+"&input'.($i+1).'="+($("#input'.($i+1).'").prop("checked") ? 1 : 0)';
				else
					$var_inputs .='+"&input'.($i+1).'="+$("#input'.($i+1).'").val()';
				
			}
		  }
	      echo 'var data = '.$var_inputs; 
		  ?>
		  
		  $.ajax({
       		url: "procesar.php",  
       		data: data,  
        	type: "POST",               
       	    cache: false,
        	success: function(data, textStatus, jqXHR){
			var ret = jQuery.parseJSON(data);
        		loaderContainer.ajaxLoaderRemove();
				if (ret["error"]!=undefined){
					clear_outputs()
					alert(ret["error"],ret["err_det"])
					return;
				}
				var outputs = ret;
				if (outputs!=null){
				<?php
				for ($i=0;$i<count($demo->outputs);$i++){
					$type=$demo->outputs[$i]->type;
						if ($type=="display"){
							echo '$("#output'.($i+1).'").val(outputs["output'.($i+1).'"])'.PHP_EOL;
						}
						if ($type=="image"){					
							echo '$("#output'.($i+1).'").attr("src",outputs["output'.($i+1).'"] + "?" + (new Date()).getTime())'.PHP_EOL;
						}
						if ($type=="iframe"){					
							echo '$("#output'.($i+1).'").attr("src",outputs["output'.($i+1).'"])'.PHP_EOL;
							echo '$("#output'.($i+1).'").css("display","")'.PHP_EOL;
						}
						if ($type=="file"){
							echo '$("#output'.($i+1).'").attr("href",outputs["output'.($i+1).'"])'.PHP_EOL;					
							echo '$("#output'.($i+1).'").html(outputs["output'.($i+1).'"])'.PHP_EOL;	
							}
				}
				?>
				}

                 
        }        
    });
	}
	
function clear_outputs(){
<?php
for ($i=0;$i<count($demo->outputs);$i++){
	$type=$demo->outputs[$i]->type;
		if ($type=="display"){
			echo '$("#output'.($i+1).'").val("")'.PHP_EOL;
		}
		if ($type=="image"){					
			echo '$("#output'.($i+1).'").attr("src","")'.PHP_EOL;
		}
		if ($type=="iframe"){					
			echo '$("#output'.($i+1).'").attr("src","")'.PHP_EOL;
			echo '$("#output'.($i+1).'").css("display","")'.PHP_EOL;
		}
		if ($type=="file"){
			echo '$("#output'.($i+1).'").attr("href","#")'.PHP_EOL;					
			echo '$("#output'.($i+1).'").html("")'.PHP_EOL;	
		}
}
?>
}
	
function upload_file(input_id){
	var loaderContainer = $('#idiv'+input_id);
	loaderContainer.ajaxLoader();
	$("#form"+input_id).submit(function(e)
	{ 
	    var formObj = $(this);
	    var formURL = formObj.attr("action");
	    var formData = new FormData(this);
	    $.ajax({
	        url: formURL,
	    	type: 'POST',
	        data:  formData,
	    	mimeType:"multipart/form-data",
	    	contentType: false,
	        cache: false,
	        processData:false,
		    success: function(data, textStatus, jqXHR)
		    {
		 		loaderContainer.ajaxLoaderRemove();
				$('#input'+input_id).val(data);

		    },
		     error: function(jqXHR, textStatus, errorThrown) 
		     {
				loaderContainer.ajaxLoaderRemove();
				alert("AJAX Error");
		     }          
		    });
		    e.preventDefault(); 
	}); 
	
}

//new alert
function alert(msg,det){
var err_det;
if (!det) 
  err_det = msg
else err_det = atob(det)
$.prompt(msg, {
	title: "Error",
	buttons: { "OK": false, "Details": true },
	submit: function(e,v,m,f){
		if (v){
			win = window.open("");
			win.document.write(err_det)
		}
	}
});
}
	
</script>

<title><?php echo $demo->name;?></title>
</head>

<body>

    <div id="header" class="header">
      <table>
        <tbody>
        <tr>
          <td id="title"><?php echo $demo->name;?></td>
        </tr>
        <tr>
          <td id="subtitle"><?php echo $demo->description;?></td>
        </tr>
      </tbody></table>
      
      <div id="">Version: <span id="version"><?php echo $demo->version;?></span></div>
    </div>
    
    <div id="messages">
    </div>   
    <div id="content">

<div id="fileinput-container" class="container">
<div id="fileinput-form" method="post" action="" enctype="multipart/form-data">
<div id="demo">     
<?php
//display inputs
for ($i=0;$i<count($demo->inputs);$i++){
	$type=$demo->inputs[$i]->type;
	$name=$demo->inputs[$i]->name;
	$options=$demo->inputs[$i]->options;
	if ($type=="single"){
		$html_input = '<div class="form-field" id="idiv'.($i+1).'"><label for="input-name">'.$name.':</label><input id="input'.($i+1).'" name="input'.($i+1).'" type="text"></div>';
	}	
	if ($type=="select"){
		$html_input = '<div class="form-field" id="idiv'.($i+1).'"><label for="input-name">'.$name.':</label><select id="input'.($i+1).'" name="input'.($i+1).'" >';
        $options=str_replace("\n",";",$options);
		$options = explode(";",$options);   
        for ($j=0;$j<count($options);$j++)   
			$html_input .= '<option value="'.$options[$j].'">'.$options[$j].'</option>';	
		$html_input .= '</select></div>';
	}
	if ($type=="file"){
		$html_input = '<div class="form-field" id="idiv'.($i+1).'"><label for="fileinput-name">'.$name.':</label><div style="float:left"><input id="input'.($i+1).'" name="input'.($i+1).'" type="hidden"/></div>';
		$html_input .= '<div><form id="form'.($i+1).'" action="upload_file.php"><input type="file" id="sample'.($i+1).'" name="sample" /><input type="hidden" name="id_demo" id="id_demo" value="'.$demo->id_demo.'"/><input type="submit" onclick="upload_file('.($i+1).')" value="Upload" /></form></div>';
		$html_input .= '</div>';
        if ($options!=""){
			$html_input .= '<span style="padding-left:184px"><a target="_blank" href="'.$options.'"> Download sample data file</a></span><br><br>'; 
		}
	}
	if ($type=="check"){
		$html_input = '<div class="form-field" id="div'.($i+1).'"><label for="check-name">'.$name.':</label><input id="input'.($i+1).'" name="input'.($i+1).'" type="checkbox"/></div>';          
	}	

	echo $html_input;

}

?>

</div>
 <input class="submit-button" value="Submit" type="button" onclick="procesar()">
 </div>
 
 
 
 <div class="container" id="fileoutput-container">

 <?php
//display outputs
for ($i=0;$i<count($demo->outputs);$i++){
 	$type=$demo->outputs[$i]->type;
	$name=$demo->outputs[$i]->name;
	if ($type=="display"){
		echo '<div class="textarea"><label for="fileoutput-c"><b>'.$name.': </b></label><textarea cols="28" rows="6" name="output'.($i+1).'" readonly="readonly" id="output'.($i+1).'"></textarea></div>';
	}
	if ($type=="image"){
		echo '<div class="textarea"><label for="fileoutput-c"><b>'.$name.': </b></label><img id="output'.($i+1).'" name="output'.($i+1).'" width="100%" /><br></div>';
	}
	if ($type=="iframe"){
			echo '<div class="textarea"><label for="fileoutput-c"><b>'.$name.': </b></label><iframe id="output'.($i+1).'" name="output'.($i+1).'" style="display:none" width="100%" height="500"></iframe><br></div>';
		}
	if ($type=="file"){
		echo '<div class="textarea" style="width:100%"><label for="fileoutput-c"><b>'.$name.': </b></label><a  id="output'.($i+1).'" name="output'.($i+1).'" href="#" target="_blank" style="width:100%">-</a></div>';
	}
	echo '<br>';
 }

?>

</div>
 
 
 
</div>
    
 </div>

    <div id="footer">
    </div>
    
  </div>


</body></html>
