<?php
session_start();

include("get_demo.php");

function delTree($dir) {
    $files = array_diff(scandir($dir), array('.','..'));
    foreach ($files as $file) {
        (is_dir("$dir/$file")) ? delTree("$dir/$file") : unlink("$dir/$file");
    }
    return rmdir($dir);
}

if ($_SESSION["user"]==""){
    header("location:login.php");
}
include("config.php");

$id = $_GET["id"];

$wd_json = get_demo($id);

$query = "DELETE FROM webdemos WHERE id = ?";
$stmt = $mysqli->prepare($query);

$stmt->bind_param('i', $id);
$exec = $stmt->execute();

if ($exec) {
    $wd_dir = $wd_json->base_dir;
    $parent_dir = dirname($wd_dir);
    delTree($parent_dir);
}

header("location:list_wd.php");
