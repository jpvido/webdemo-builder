<?php
/*
 *  Copyright 2014-2016 Milton Pividori, Juan Pablo Vidocevich, Diego Milone and Georgina Stegmayer.
 *
 *  This file is part of Web-demo builder.
 *
 *  Web-demo builder is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Web-demo builder is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with Web-demo builder. If not, see <http://www.gnu.org/licenses/>.
 *
 */

if ($_FILES){
	$file="";
	if($_FILES["sample"]["name"]) {
		$id_demo = $_REQUEST["id_demo"];
		$filename = $_FILES["sample"]["name"];
		$source = $_FILES["sample"]["tmp_name"];
		$directory = "files/".$id_demo."/";
		$name = explode(".", $filename);
		$rand = str_pad(mt_rand(1,99999999),4,'0',STR_PAD_LEFT); 
		$filename = $name[0].$rand.".".$name[1];
		if (!file_exists($directory))
			mkdir($directory, 0777, true);
		$target_path = $directory.$filename; 
		if(move_uploaded_file($source, $target_path)) {	
			$file = $target_path;		
		}
	}
	echo $file;
}
?>
