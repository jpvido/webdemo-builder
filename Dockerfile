FROM docker:20.10.6 AS docker

FROM php:5.5-apache
RUN docker-php-ext-install mysqli pdo pdo_mysql
RUN docker-php-ext-install opcache
RUN docker-php-ext-install json

RUN apt-get update
RUN apt-get install -y libzip-dev
RUN docker-php-ext-install zip

COPY "php_config/php.ini" "$PHP_INI_DIR/php.ini"
COPY "php_config/conf.d/*" "$PHP_INI_DIR/conf.d/"

COPY --from=docker /usr/local/bin/docker /usr/local/bin/

RUN mkdir /wdb_scripts
RUN chmod -R 777 /wdb_scripts
WORKDIR /var/www/html
