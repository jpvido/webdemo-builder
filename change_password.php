<?php
include("config.php");

session_start();
if ($_SESSION["user"]==""){
    header("location:login.php");
}

// GET USER DATA
$name = "";
$user = "";

$query = "SELECT name, user from users where id = ?";
$stmt = $mysqli->prepare($query);
$stmt->bind_param('i', $_SESSION["user_id"]);
$exec = $stmt->execute();

if ($exec) {
    $stmt->bind_result($name, $user);
    $stmt->fetch();
}
$stmt->close();

// UPDATE PASSWORD
if ($_POST) {
    $valid = validate_old_password($mysqli, $_SESSION["user_id"], $_POST["password"]);
    if ($valid) {
        $new_pass_enc = md5($_POST["new_password"]);
        $query = "UPDATE users SET pass = ? WHERE id = ?";
        $stmt = $mysqli->prepare($query);
        $stmt->bind_param('si', $new_pass_enc, $_SESSION["user_id"]);
        $exec = $stmt->execute();
        if ($exec) {
            header("location:index.php");
        } else {
            echo '<script>alert("An error occurred during password change")</script>';
        }
        $stmt->close();
    } else {
        echo '<script>alert("Old password is wrong")</script>';
    }
}

function validate_old_password($mysqli, $user_id, $old_password) {
    $query = "SELECT pass from users where id = ?";
    $stmt = $mysqli->prepare($query);
    $stmt->bind_param('i', $user_id);
    $exec = $stmt->execute();
    if ($exec) {
        $stmt->bind_result($saved_old_pass);
        $stmt->fetch();
        $stmt->close();
        return $saved_old_pass == md5($old_password);
    }
    $stmt->close();
    return False;
}

?>

<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="webdemo builder">
    <meta name="author" content="sinc">

    <title>Webdemo Builder 2</title>

    <!-- Custom fonts for this template-->
    <link href="vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
    <link
        href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i"
        rel="stylesheet">

    <!-- Custom styles for this template-->
    <link href="css/sb-admin-2.css" rel="stylesheet">

</head>


<body id="page-top">

    <!-- Page Wrapper -->
    <div id="wrapper">

        <?php include('sidebar.php'); ?>
        <!-- Content Wrapper -->
        <div id="content-wrapper" class="d-flex flex-column">

            <!-- Main Content -->
            <div id="content">

                <?php include('topbar.php'); ?>

                <!-- Begin Page Content -->
                <div class="container-fluid">

                    <!-- Page Heading -->
                    <div class="d-sm-flex align-items-center justify-content-between mb-4">
                        <h1 class="h3 mb-0 text-gray-800">Change Password</h1>
                     
                    </div>

                     <div class="card shadow mb-4">
                        <div class="card-header py-3">
                            <h6 class="m-0 font-weight-bold text-primary">User data</h6>
                        </div>
                        <div class="card-body">
                            
                        <div class="card-body">
                            <form method="post">
                            <div class="form-group">
                                <label for="name">Name</label>
                                <input type="text" class="form-control" name="name" id="name"
                                       value="<?php echo htmlspecialchars($name); ?>" disabled>
                                
                            </div>
                           <div class="form-group">
                                <label for="user">User</label>
                                <input type="text" class="form-control" name="user" id="user"
                                       value="<?php echo htmlspecialchars($user); ?>" disabled>
                                
                            </div>
                             <div class="form-group">
                                <label for="password">Old Password</label>
                                <input id="password" class="form-control" name="password" type="password" pattern="^\S{6,}$" required>

                            </div>
                            <div class="form-group">
                                <label for="new_password">New Password</label>
                                <input id="new_password" class="form-control" name="new_password" type="password"
                                       pattern="^\S{6,}$"
                                       onchange="this.setCustomValidity(this.validity.patternMismatch ? 'Must have at least 6 characters' : ''); if(this.checkValidity()) form.password_two.pattern = this.value;" required>

                            </div>
                            <div class="form-group">
                                <label for="pass">Verify New Password</label>
                                <input id="password_two" class="form-control" name="password_two" type="password" pattern="^\S{6,}$" onchange="this.setCustomValidity(this.validity.patternMismatch ? 'Please enter the same Password as above' : '');" required>

                            </div>
                            
                            <button type="submit" class="btn btn-primary">Save</button>

                            </form>
                        </div>

                        </div>
                    </div>
                    
                    


                </div>
                <!-- /.container-fluid -->

            </div>
            <!-- End of Main Content -->

            <!-- Footer -->
            <footer class="sticky-footer bg-white">
                <div class="container my-auto">
                    <div class="copyright text-center my-auto">
                        <span>Webdemo Builder 2021</span>
                    </div>
                </div>
            </footer>
            <!-- End of Footer -->

        </div>
        <!-- End of Content Wrapper -->

    </div>
    <!-- End of Page Wrapper -->

    <!-- Scroll to Top Button-->
    <a class="scroll-to-top rounded" href="#page-top">
        <i class="fas fa-angle-up"></i>
    </a>

    <!-- Logout Modal-->
    <div class="modal fade" id="logoutModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
        aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Ready to Leave?</h5>
                    <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <div class="modal-body">Select "Logout" below if you are ready to end your current session.</div>
                <div class="modal-footer">
                    <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
                    <a class="btn btn-primary" href="logout.php">Logout</a>
                </div>
            </div>
        </div>
    </div>

    <!-- Bootstrap core JavaScript-->
    <script src="vendor/jquery/jquery.min.js"></script>
    <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

    <!-- Core plugin JavaScript-->
    <script src="vendor/jquery-easing/jquery.easing.min.js"></script>

    <!-- Custom scripts for all pages-->
    <script src="js/sb-admin-2.min.js"></script>

    <!-- Page level plugins -->
    <script src="vendor/chart.js/Chart.min.js"></script>

    <!-- Page level custom scripts -->
    <script src="js/demo/chart-area-demo.js"></script>
    <script src="js/demo/chart-pie-demo.js"></script>
    
    <script type="text/javascript" src="jquery.ajaxLoader.js"></script>
    <script type="text/javascript" src="jquery.smooth-scroll.js"></script>

    <script>



    </script>
</body>


</html>
