<?php
session_start();
if ($_SESSION["user"]==""){
    header("location:login.php");
}
include("config.php");

?>
<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="webdemo builder">
    <meta name="author" content="sinc">

    <title>Webdemo Builder 2</title>

    <!-- Custom fonts for this template-->
    <link href="vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
    <link
        href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i"
        rel="stylesheet">

    <!-- Custom styles for this template-->
    <link href="css/sb-admin-2.css" rel="stylesheet">

</head>


<body id="demo_body">

    <!-- Page Wrapper -->
    <div id="wrapper">

        <?php include('sidebar.php'); ?>

        <!-- Content Wrapper -->
        <div id="content-wrapper" class="d-flex flex-column">

            <!-- Main Content -->
            <div id="content">


            <div id="loader" style="z-index:999;position:fixed;display:none; width:100%;height:100%; background-color:black; opacity:0.5">
            <div style="margin-left:40%;margin-top:20%;" id="loader" class="spinner-border" role="status">
            <span class="sr-only">Loading...</span>
            </div>
               </div>

                <?php include('topbar.php'); ?>

                <!-- Begin Page Content -->
                <div class="container-fluid">


                <?php
                    $query = "SELECT * from images_availables ";
                    $res = $mysqli->query($query);
                    
                ?>
                    <!-- Page Heading -->
         
                        <h1 class="h3 mb-0 text-gray-800">New webdemo</h1>
                    <p>
                 
                    <div class="card shadow mb-4">
                        <div class="card-header py-3">
                            <h6 class="m-0 font-weight-bold text-primary">General description</h6>
                        </div>
                        <div class="card-body">
                            <form>
                            <div class="form-group">
                                <label for="wd_name">Name</label>
                                <input type="text" class="form-control" name="name" id="name">
                                
                            </div>
                           <div class="form-group">
                                <label for="wd_desc">Description</label>
                                <input type="text" class="form-control" name="desc" id="desc">
                                
                            </div>
                             <div class="form-group">
                                <label for="wd_version">Version</label>
                                <input type="text" class="form-control" name="ver" id="ver">
                                
                            </div>
                            
                            </form>
                        </div>
                    </div>
                    
                    <div id="script-content">
                    
                    <div class="card shadow mb-4">
                        <div class="card-header py-3">
                            <h6 class="m-0 font-weight-bold text-primary">Source code</h6>
                        </div>
                        <div class="card-body">
                        
                        <form id="form_script" name="form_script" action="#">	
                        <input type="hidden" name="id_demo" id="id_demo" />
                            <div class="form-group">
                                <label for="wd_type">Type</label>
                                <select class="form-control" id="type-script" name="type-script"  onchange="type_script_change()">
                                <option value="python">Python</option>   
                                <option value="r">R</option>   
                                <option value="matlab">Matlab</option>  
                                </select>
                                
                            </div>

                            <div class="form-group">
                                <label for="wd_file">Zip File (Code)</label>
                                <input type="file" class="form-control" accept="application/zip" name="fileinput-script" id="fileinput-script">

                            </div>

                           <div class="form-group">
                                <label for="wd_env">Docker container id</label>
                                <!-- <input type="text" class="form-control" id="virtual_env" name="virtual_env"> -->
                                <select class="form-control"  id="virtual_env" name="virtual_env" >
                                <?php while ($row = $res->fetch_assoc()) {?>
                                    <option value="<?php echo $row['name']  ?>" ><?php echo $row['name']  ?></option>
                                <?php } ?>
                                </select>    

                            </form>

                            <p>
                                <button class="btn btn-link" type="button" data-toggle="collapse" data-target="#collapseExample" aria-expanded="false" aria-controls="collapseExample">
                                    Add another docker image
                                </button>
                                <div class="collapse" id="collapseExample">
                                <div class="card card-body">
                                    <form class="form-inline" method="post" action="add_image_docker.php">
                                        <label for="docker_image">new docker container id: </label>
                                        <input type="text" class="form-control col-6" name="docker_image" required>
                                        <button class="btn btn-success" type="submit" >Add</button>

                                        <p>After click in <b>Add</b>, you will found the image in the list (Custom virtual environment).</p>
                                    </form>
                                </div>
                                </div>
                            </p>
                        
                        </div>

                    </div>

                    </div>
                    
                    <div id="options-content" style="display:none">
                        <div class="card shadow mb-4">
                            <div class="card-header py-3">
                                <h6 class="m-0 font-weight-bold text-primary">Main function</h6>
                            </div>
                            <div class="card-body">
                                                        
                                <div class="form-group" id="files" style="display:none">           
                                <label for="file-script">Script file:</label>
                                    <select id="file-script" class="form-control" name="file-script" type="text" onchange="load_functions()">
                                    </select>
                              </div> 

                                <div class="form-group" id="functions" style="display:none">           
                                <label for="function-script">Function:</label>
                                    <select id="function-script" class="form-control" name="function-script" onchange="load_inputs_html()" type="text">
                                    </select>
                                </div> 
                 
                            </div>

                        </div>
                    </div>


                    <div id="inputs" > 
                    
                    </div>

                    <div id="outputs" >
                    </div>



 <button type="button" id="main_button_upload" onclick="upload_code()" class="btn btn-primary">Upload</button>
 <button type="button" id="main_button_generate" style="display:none" onclick="generate_demo()" class="btn btn-primary">Generate</button>                        
                </div>
                <!-- /.container-fluid -->

            </div>
            <!-- End of Main Content -->

            <!-- Footer -->
            <footer class="sticky-footer bg-white">
                <div class="container my-auto">
                    <div class="copyright text-center my-auto">
                        <span>Webdemo Builder 2021</span>
                    </div>
                </div>
            </footer>
            <!-- End of Footer -->

        </div>
        <!-- End of Content Wrapper -->

    </div>
    <!-- End of Page Wrapper -->

    <!-- Scroll to Top Button-->
    <a class="scroll-to-top rounded" href="#page-top">
        <i class="fas fa-angle-up"></i>
    </a>

    <!-- Logout Modal-->
    <div class="modal fade" id="logoutModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
        aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Ready to Leave?</h5>
                    <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <div class="modal-body">Select "Logout" below if you are ready to end your current session.</div>
                <div class="modal-footer">
                    <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
                    <a class="btn btn-primary" href="logout.php">Logout</a>
                </div>
            </div>
        </div>
    </div>

    <!-- Bootstrap core JavaScript-->
    <script src="vendor/jquery/jquery.min.js"></script>
    <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

    <!-- Core plugin JavaScript-->
    <script src="vendor/jquery-easing/jquery.easing.min.js"></script>

    <!-- Custom scripts for all pages-->
    <script src="js/sb-admin-2.min.js"></script>

    <!-- Page level plugins -->
    <script src="vendor/chart.js/Chart.min.js"></script>

    <!-- Page level custom scripts -->
    <script src="js/demo/chart-area-demo.js"></script>
    <script src="js/demo/chart-pie-demo.js"></script>

    
    <script type="text/javascript" src="jquery.smooth-scroll.js"></script>

    <script language="javascript">	

<?php
$wdb_config = parse_ini_file("templates/config.ini");
echo "var type_script_and_image = {'matlab':'".$wdb_config["base_matlab_image"]."', 'python':'".$wdb_config["base_python2_image"]."', 'r':'".$wdb_config["base_r3_image"]."'}";
?>
	
var file_data=null;
var id_demo_rand = null;

$(document).ready(function(){
    //$('.tooltip').tooltipster();

    // Update virtual environment image id
    type_script_change();

    // Set the default virtual env value (useful when resetting forms)
    var lang_selected = $('#type-script').val();
    $('#virtual_env').attr('value', type_script_and_image[lang_selected.toLowerCase()]);
});

//Upload zip
function type_script_change() {
    var lang_selected = $('#type-script').val();

    $('#virtual_env').val(type_script_and_image[lang_selected.toLowerCase()]);
    //virtual_env_disable();
}

function virtual_env_enable() {
    $('#virtual_env').removeAttr('disabled');
}

function virtual_env_disable() {
    $('#virtual_env').attr('disabled', 'disable');
}

function source_code_options_disable() {
   $('#type-script').attr('disabled', 'disabled');
   $('#virtual_env').attr('disabled', 'disabled');
   $('#fileinput-script').attr('disabled', 'disabled');
   $('#enable_virtual_env').attr('disabled', 'disabled');
}

function source_code_options_enable() {
   $('#type-script').removeAttr('disabled');
   $('#virtual_env').removeAttr('disabled');
   $('#fileinput-script').removeAttr('disabled');
   $('#enable_virtual_env').removeAttr('disabled');
}

function upload_code(){
	id_demo_rand =  Math.floor((Math.random() * (99999999 - 10000000 + 1)) + 10000000);
	$('#id_demo').val(id_demo_rand); 
	 
	//var loaderContainer = $('div#script-content');
	//loaderContainer.ajaxLoader();
    loader();
    var formObj = $('#form_script');
	var formURL = "#";
	if ($('#type-script').val()=='matlab')
		formURL = "upload_zip_m.php"
	if ($('#type-script').val()=='r')
		formURL = "upload_zip_r.php"
	if ($('#type-script').val()=='python')
		formURL = "upload_zip_py.php"
    
    var formData = new FormData(document.getElementById('form_script'));
   
    $.ajax({
        url: formURL,
    	type: 'POST',
        data:  formData,
    	mimeType:"multipart/form-data",
    	contentType: false,
        cache: false,
        processData:false,
	    success: function(data, textStatus, jqXHR)
	    {
            //loaderContainer.ajaxLoaderRemove();
			//response
			noloader();
			file_data = jQuery.parseJSON(data);
			if (file_data["error"]!=0){
				alert(file_data["message"])
				return;
			}
			load_files();
            $('#main_button_upload').hide();
            $('#main_button_generate').show();
                        $.smoothScroll({
                          scrollTarget: '#options-content'
                        });

			source_code_options_disable();
	    },
	     error: function(jqXHR, textStatus, errorThrown) 
	     {
			//loaderContainer.ajaxLoaderRemove();
            noloader();
			alert("AJAX Error");
	     }          
	    });

	
}
//upload file input
function upload_file(input_id){
	//var loaderContainer = $('#inputs');
	//loaderContainer.ajaxLoader();
    loader();
	$("#form"+input_id).submit(function(e)
	{ 
	    var formObj = $(this);
	    var formURL = formObj.attr("action");
	    var formData = new FormData(this);
	    $.ajax({
	        url: formURL,
	    	type: 'POST',
	        data:  formData,
	    	mimeType:"multipart/form-data",
	    	contentType: false,
	        cache: false,
	        processData:false,
		    success: function(data, textStatus, jqXHR)
		    {
		 		//loaderContainer.ajaxLoaderRemove();
                noloader();
                let lastFileIndex = window.location.href.lastIndexOf('/');
                let baseLocation = window.location.href.substr(0, lastFileIndex);
				$('#options'+input_id).val(`${baseLocation}/${data}`);
		    },
		     error: function(jqXHR, textStatus, errorThrown) 
		     {
				//loaderContainer.ajaxLoaderRemove();
                noloader();
				alert("AJAX Error");
		     }          
		    });
		    e.preventDefault(); 
	}); 
	
}

//load source code files
function load_files(){
	if (file_data==null) return;
	
	if (file_data.files.length ==0) {
		alert("No code files found in root of code directory");
		return;
	}
	$('#options-content').css( "display", "" );
	$('#files').css( "display", "" );
	$("#file-script").html("");	
	for (var i=0;i<file_data.files.length;i++){
		$("#file-script").append('<option value="'+i+'">'+file_data.files[i].name+'</option>');
	}
	
	if (file_data.files[0].functions.length ==0) {
		alert("No functions found in this file");
		return;
	}
	$('#functions').css( "display", "" );
	$("#function-script").html("");
	for (var i=0;i<file_data.files[0].functions.length;i++){
		$("#function-script").append('<option value="'+i+'">'+file_data.files[0].functions[i].name+'</option>');
	}
	load_inputs(0,0);
	load_outputs(0,0);
}

//load functions
function load_functions(){

	var j = parseInt($('#file-script').val());
	if (file_data.files[j].functions.length ==0) {
		alert("No functions found in this file");
		return;
	}
	$('#functions').css( "display", "" );
	$("#function-script").html("");
	for (var i=0;i<file_data.files[j].functions.length;i++){
		$("#function-script").append('<option value="'+i+'">'+file_data.files[j].functions[i].name+'</option>');
	}
	load_inputs(j,0);
	load_outputs(j,0);
}
//display inputs
function load_inputs_html(){
  var file_id = parseInt($('#file-script').val());
  var fun_id = parseInt($('#function-script').val());
  load_inputs(file_id, fun_id);
  load_outputs(file_id, fun_id);
}
//display inputs
function load_inputs(file_id,fun_id){

    inputs = file_data.files[file_id].functions[fun_id].inputs;
    var inputsHMTL = ""
	for (var i=0;i<inputs.length;i++){
                          
        inputsHMTL+='<div class="card shadow mb-4">';
        inputsHMTL+='<div class="card-header py-3">';
        inputsHMTL+='<h6 class="m-0 font-weight-bold text-primary">Input '+(i+1)+':'+inputs[i]+'</h6>';
        inputsHMTL+='</div>';
        inputsHMTL+='<div class="card-body">';

		inputsHMTL+='<div id="fileinput-container" class="admin-box">'
		inputsHMTL+='<div class="form-group">'
		inputsHMTL+='<label for="desc'+i+'">Description:</label>'
		inputsHMTL+='<input type="text" class="form-control" class="form-group" id="desc'+i+'" name="desc'+i+'" value="'+inputs[i]+'"/>'
              //  inputsHMTL+=' <div class="input_tooltip tooltipster-icon" title="Describe this input."></div>'
		inputsHMTL+='</div>'  
		inputsHMTL+='<div class="form-group">'           
		inputsHMTL+='<label for="type'+i+'">Type:</label>'
		inputsHMTL+='<select id="type'+i+'" class="form-control" name="type'+i+'" type="text">'
		inputsHMTL+='<option value="single">Simple Input</option>'   
		inputsHMTL+='<option value="select">Select</option>'
		inputsHMTL+='<option value="file">File</option>' 
		inputsHMTL+='<option value="check">Checkbox</option>'  
		inputsHMTL+='</select>'    
                //inputsHMTL+=' <div class="input_type_tooltip tooltipster-icon"></div>'
		inputsHMTL+='</div>'
		inputsHMTL+='<div class="form-group">'
		inputsHMTL+='<label for="options'+i+'">Options:</label>'
		inputsHMTL+='<textarea class="form-control" id="options'+i+'" name="options'+i+'"></textarea>'  
		inputsHMTL+='</div>'
		inputsHMTL+='<div class="form-group">'
		inputsHMTL+='<label for="sample'+i+'">Sample file:</label>'
		inputsHMTL+='<form id="form'+i+'" action="upload_file.php"><input type="file" class="form-control" id="sample'+i+'" name="sample" /><input type="hidden" name="id_demo" id="id_demo" value="'+id_demo_rand+'" /><input type="submit" onclick="upload_file('+i+')" value="Upload" /> <div class="input_tooltip tooltipster-icon" title="If you have specified type File, you can provide a sample file here."></div></form>'
		inputsHMTL+='</div>'   		 
		inputsHMTL+='</div>'

        inputsHMTL+='</div>';
        inputsHMTL+='</div>';
	}

	$('#inputs').html(inputsHMTL);

	//$('.input_tooltip').tooltipster();

    /*
	$('.input_type_tooltip').tooltipster({
		content: $('<p>Choose the input type. Types include:<br /> <ul><li><b>Simple Input:</b> An input parameter.</li> <li><b>Select:</b> A list   only one item can be selected. The items should be written in the Options box (one per line).</li> <li><b>File:</b> The input filename will be passed to the main function as a string. A sample file can be provided below.</li> <li><b>Checkbox:</b> An input parameter representing a boolean (1 or 0).</li>')
	});*/
}
//display outputs

function load_outputs(file_id,fun_id){
    outputs = file_data.files[file_id].functions[fun_id].outputs;
    var outputsHMTL = ""
	for (var i=0;i<outputs.length;i++){
        outputsHMTL+='<div class="card shadow mb-4">';
        outputsHMTL+='<div class="card-header py-3">';
        outputsHMTL+='<h6 class="m-0 font-weight-bold text-primary">Output '+(i+1)+':'+outputs[i]+'</h6>';
        outputsHMTL+='</div>';
        outputsHMTL+='<div class="card-body">';

	    outputsHMTL+='<div id="fileinput-container" class="admin-box">'
        outputsHMTL+='<div class="form-group">'
        outputsHMTL+='<label for="desc_out'+i+'">Description:</label>'
        outputsHMTL+='<input type="text" class="form-control" id="desc-out'+i+'" name="desc-out'+i+'" value="'+outputs[i]+'" />'
        //outputsHMTL+=' <div class="output_tooltip tooltipster-icon" title="Describe this output."></div>'
        outputsHMTL+='</div>'    
        outputsHMTL+='<div class="form-group">'           
        outputsHMTL+='<label for="type-out'+i+'">Type:</label>'
        outputsHMTL+='<select  class="form-control" id="type-out'+i+'" name="type-out'+i+'" type="text">'
        outputsHMTL+='<option value="display">Display</option>'
        outputsHMTL+='<option value="image">Image</option>'
        outputsHMTL+='<option value="file">File</option>' 
	    outputsHMTL+='<option value="iframe">IFrame</option>'   
        outputsHMTL+='</select>'
        //outputsHMTL+=' <div class="output_type_tooltip tooltipster-icon"></div>'
        outputsHMTL+='</div>'        
		outputsHMTL+='</div>'

        outputsHMTL+='</div>';
        outputsHMTL+='</div>';
	}

	$('#outputs').html(outputsHMTL);

	//$('.output_tooltip').tooltipster();
    /*
	$('.output_type_tooltip').tooltipster({
		content: $('<p>Choose the output type. Types include:<br /> <ul><li><b>Display:</b> It is the simplest one, just display the value (string or number).</li> <li><b>Image:</b> Show an image. The web-demo function must return the image path.</li> <li><b>File:</b> Show a link to download the file. The web-demo function must return the file path.</li> <li><b>IFrame:</b> Show the HTML output. The web-demo function must return the HTML file path.</li>')
	});
    */
}

var inputs;
var outputs;

//generate json demo
function generate_demo(){

	if ($('#name').val()==""){
		alert("Complete all fields")
		return;
	}
	if ($('#desc').val()==""){
		alert("Complete all fields")
		return;
	}
	if ($('#ver').val()==""){
		alert("Complete all fields")
		return;
	}
	if (file_data==null){
		alert("Complete all fields")
		return;
	}

	demo = 	{}
	demo['id_demo'] = $('#id_demo').val();
	demo['name'] = $('#name').val();
	demo['description'] = $('#desc').val();
	demo['version'] = $('#ver').val();
	demo['virtual_env'] = $('#virtual_env').val();
	demo['filescript'] = $('#file-script').children(':selected').text();
	
	var f = parseInt($('#file-script').val());
	demo['base_dir'] = file_data.files[f].base_dir;

	var fn = parseInt($('#function-script').val());
	demo['function'] = file_data.files[f].functions[fn].name;

	var demo_inputs = new Array()

	for (var i=0;i<inputs.length;i++){
		item = {}
		item ["name"] = $('#desc'+i).val();
		item ["type"] = $('#type'+i).val();
		item ["options"] = $('#options'+i).val();
		demo_inputs.push(item);
		}
	var demo_outputs = new Array()
	for (var i=0;i<outputs.length;i++){
		item = {}
		item ["name"] = $('#desc-out'+i).val();
		item ["type"] = $('#type-out'+i).val();
		demo_outputs.push(item);
		}	

	demo['inputs'] = demo_inputs;
	demo['outputs'] = demo_outputs;

	send_demo(JSON.stringify(demo));

}

function send_demo(demo){
	    var url_create = "#";
		if ($('#type-script').val()=='matlab')
			url_create = "create_demo_m.php"
	    if ($('#type-script').val()=='r')
			url_create = "create_demo_r.php"
		if ($('#type-script').val()=='python')
			url_create = "create_demo_py.php"
		
	      //var loaderContainer = $('#demo_body');
		  //loaderContainer.ajaxLoader();
          loader();
	      var data = "demo_json="+demo
		  $.ajax({
       		url: url_create,  
                async: false,
       		data: data,  
        	type: "POST",               
       	    cache: false,
        	success: function (html) {    
        		noloader();
				demo_url=html;
				if (demo_url!=""){
				   //open_demo();
                    location.href="list_wd.php"
                }
        }        
    });
}
var demo_url;

function open_demo(){
	window.open(demo_url);
}
/*
//new alert
function alert(msg,det){
var err_det;
if (det=="") 
  err_det = msg
else err_det = det
$.prompt(msg, {
	title: "Error",
	buttons: { "OK": false, "Details": true },
	submit: function(e,v,m,f){
		if (v){
			win = window.open("");
			win.document.write(err_det)
		}
	}
});
}
*/

function loader(){
    $('#loader').css('display','')
}
function noloader(){
    $('#loader').css('display','none')
}
</script>

</body>


</html>
