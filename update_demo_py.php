<?php
/*
 *  Copyright 2014-2016 Milton Pividori, Juan Pablo Vidocevich, Diego Milone and Georgina Stegmayer.
 *
 *  This file is part of Web-demo builder.
 *
 *  Web-demo builder is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Web-demo builder is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with Web-demo builder. If not, see <http://www.gnu.org/licenses/>.
 *
 */
session_start();
//crea json del demo y copia todos los archivos necesarios al directorio del web demo

if ($_POST){
    try {

	$demo_json = $_POST["demo_json"];
	$demo = json_decode($demo_json);
	
	$file = fopen($demo->base_dir."demo.json","w+");	
	fwrite($file, $demo_json);
	fclose($file);

	include("config.php");

	$sql = "UPDATE webdemos SET code = '".$demo->id_demo."' , name = '".$demo->name."' , json = '".$demo_json."' , date = now() , user_id = ".$_SESSION["user_id"]." WHERE id = ".$_REQUEST["id"];
    echo $sql;
	$result = $mysqli->query($sql);

	
	copy("templates/nopwd.htaccess",$demo->base_dir.".htaccess"); 
	copy("templates/run_docker.php",$demo->base_dir."run_docker.php");
	copy("templates/run.sh",$demo->base_dir."run.sh");
	copy("templates/config.ini",$demo->base_dir."config.ini");  
	copy("templates/index.php",$demo->base_dir."index.php");
	copy("templates/procesarPYTHON.php",$demo->base_dir."procesar.php");
	copy("templates/upload_file.php",$demo->base_dir."upload_file.php");
	copy("templates/jquery-impromptu.min.js",$demo->base_dir."jquery-impromptu.min.js");
	copy("templates/jquery-impromptu.min.css",$demo->base_dir."jquery-impromptu.min.css");
	copy("ajax-loader.gif",$demo->base_dir."ajax-loader.gif");
	copy("jquery.ajaxLoader.js",$demo->base_dir."jquery.ajaxLoader.js");
	copy("jquery.js",$demo->base_dir."jquery.js");
	copy("style.css",$demo->base_dir."style.css");

	echo $demo->base_dir;
	
	} catch (Exception $e) {
		echo $e;
	}
	
}

?>
