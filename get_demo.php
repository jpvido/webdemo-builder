<?php

function get_demo($id) {
    include("config.php");
    $query = "SELECT json from webdemos where id = ?";
    $stmt = $mysqli->prepare($query);
    $stmt->bind_param('i', $id);
    $exec = $stmt->execute();
    if ($exec) {
        $stmt->bind_result($wd_json);
        $stmt->fetch();
        return $wd_json;
    } else {
        return null;
    }
}


function isOwnWebDemo($id){
    include("config.php");
    $query = "SELECT user_id from webdemos where id = ?";
    $stmt = $mysqli->prepare($query);
    $stmt->bind_param('i', $id);
    $exec = $stmt->execute();
    if ($exec) {
        $stmt->bind_result($wd_json);
        $stmt->fetch();
        return $wd_json;
    } else {
        return null;
    }
}
