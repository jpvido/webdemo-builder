<?php
/*
 *  Copyright 2014-2016 Milton Pividori, Juan Pablo Vidocevich, Diego Milone and Georgina Stegmayer.
 *
 *  This file is part of Web-demo builder.
 *
 *  Web-demo builder is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Web-demo builder is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with Web-demo builder. If not, see <http://www.gnu.org/licenses/>.
 *
 */

//upload zip and parse source code
// Python, Matlab and R

require 'fileutils.php';

define('KB', 1024);
define('MB', 1048576);
define('GB', 1073741824);
define('TB', 1099511627776);


function upload_zip($extension, $process_file_functions) {
    if ($_FILES) {
        try{
            $files_r = null;
            $error=0;
            $message="";
            $m_files = null;

            // Check file size
            if ($_FILES["fileinput-script"]["size"] > 500*MB) {
                throw new Exception("The file you've uploaded exceeds the size limit (500 MB).");
            }

            if($_FILES["fileinput-script"]["name"]) {
                $id_demo = $_REQUEST["id_demo"];
                $filename = $_FILES["fileinput-script"]["name"];
                $source = $_FILES["fileinput-script"]["tmp_name"];
                $type = $_FILES["fileinput-script"]["type"];
                $directory = "scripts/".$id_demo."/";
                $base_dir = "";
                $name = explode(".", $filename);
                $accepted_types = array('application/zip', 'application/x-zip-compressed', 'multipart/x-zip', 'application/x-compressed');
                foreach($accepted_types as $mime_type) {
                    if($mime_type == $type) {
                        $okay = true;
                        break;
                    }
                }

                $continue = strtolower($name[(count($name)-1)]) == 'zip' ? true : false;
                if(!$continue) {
                    $error=1;
                    $message = "The file you are trying to upload is not a .zip file. Please try again.";
                }

                if (mkdir($directory, 0777, true) == false) {
                    error_log("MKDIR error creating scripts folder. Check permissions.");
                }

                // Create temporal dir
                $temp_dir = sys_get_temp_dir()."/wdb".$id_demo."/";
                mkdir($temp_dir, 0777, true);
                $target_path = $temp_dir.$filename;

                if(move_uploaded_file($source, $target_path)) {
                    $zip = new ZipArchive();
                    $x = $zip->open($target_path);
                    if ($x === true) {
                        $zip->extractTo($temp_dir);
                        $zip->close();
                        $dh  = opendir($temp_dir);
                        $zipfilename = $_FILES["fileinput-script"]["name"];
                        while (false !== ($filename = readdir($dh))) {
                            if ($filename!="." and $filename!=".." and $filename!=$zipfilename)
                                $files[] = $filename;
                        }
                        // If there are more than one file in the zip or there is only one file
                        // which is not a directory, then copy all of them into a single directory
                        if ( count($files) > 1 or (count($files) == 1 and !is_dir($temp_dir.$files[0])) ){
                            recurse_copy($temp_dir, $directory.$name[0]);
                        } elseif ( count($files) == 1 and is_dir($temp_dir.$files[0]) ) {
                            recurse_copy($temp_dir.$files[0], $directory.$name[0]);
                        } else {
                            throw new Exception('No files were found in the zip archive.');
                        }
                        rename($target_path, $directory.$zipfilename);

                        delete_dir($temp_dir);
                        unset($files);

                        // Continue
                        if ($dh = opendir($directory)) {
                            while (false !== ($filename = readdir($dh))) {
                                if ($filename!="." and $filename!=".." and $filename!=$zipfilename)
                                    $files[] = $filename;
                            }
                            if (count($files)==1) {
                                $base_dir = $directory.$files[0];
                                $dh  = opendir($directory.$files[0]);
                                while (false !== ($filename = readdir($dh))) {
                                    if ($filename!="." and $filename!=".."){
                                        $filen = explode(".", $filename);
                                        if (count($filen)!=2) continue;
                                        $ext = $filen[1];
                                        if (strtolower($ext)==$extension){
                                            $m_files[]=$filename;
                                        }
                                    }
                                }

                                if (count($m_files)>0){
                                    for ($i=0;$i<count($m_files);$i++){
                                        if ($file = fopen($base_dir."/".$m_files[$i], "r")){
                                            $funciones = $process_file_functions($file);
                                            if (count($funciones)>0){
                                                $file_r["name"] = $m_files[$i];
                                                $file_r["base_dir"] = $base_dir."/";
                                                $file_r["functions"] = $funciones;
                                                $files_r[]=$file_r;
                                            }
                                        }
                                        else{
                                            $error=1;
                                            $message = "Error open file";
                                        }
                                    }
                                }
                                else{
                                    $error=1;
                                    $message = "No .py files found";
                                }

                            }else{
                                $error=1;
                                $message = "No files found in zip file";
                            }
                        }

                    }

                } else {
                    $error=1;
                    $message = "There was a problem with the upload. Please try again.";
                }

            }
            else{
                $error=1;
                $message = "No zip files";
            }
            if ($error == 1) {
                $res["error"] = 1;
                $res["message"] = $message;
                $res["files"] = "null";
            } else {
                $res["error"] = 0;
                $res["message"] = $message;
                $res["files"] = $files_r;
            }
            return $res;
        }
        catch (Exception $e){
            $res["error"] = 1;
            $res["message"] = $e->getMessage();
            $res["files"] = "null";
            return $res;
        }
    }
    else {
        $res["error"] = 1;
        $res["message"] = "No files here";
        $res["files"] = "null";
        return $res;
    }
}
?>

