<?php
$root_pass = getenv('MYSQL_ROOT_PASSWORD');
$database = getenv('MYSQL_DATABASE');
$mysqli = new mysqli('wdb2-mysql', 'root', $root_pass, $database);

if ($mysqli->connect_errno) {

    die("Database connect error");

}

?>
