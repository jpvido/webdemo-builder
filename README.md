# Webdemo Builder
## Development

To run Webdemo Builder 2, run:

```
docker-compose up
```

But first!

### Check permissions
Docker containers must have **write** permission over folders: 
`scripts`, `dind`, `files` and `mysql`.

### Configure MySQL
Save your Mysql password and database name in .env file.

The .env file should look like:

```
MYSQL_ROOT_PASSWORD=root_password
MYSQL_DATABASE=database_name
```

### Provide initial data

Copy your MySQL dump file (`.sql`) into `mysql/` folder. 
It will be loaded when the container starts.


